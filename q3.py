import pandas
import numpy 

class TreeNode:
    def __init__(self):
        self.attri = self.val = self.l_arr = self.data_list = None

class DecisionTree:
    def __init__(self):
        self.df = self.test_df = self.dtree = None        
        self.unique_vals = self.dcols = self.dtypes = []
        self.max_depth,self.min_thresh,self.skip = 4,0.001,10
        
    def load_data(self, train_filename):
        self.df = pandas.read_csv(train_filename, delimiter=',')
        self.dtypes, self.dcols = list(self.df.dtypes)[1:-1], list((self.df).columns[1:-1])
        
        for i,typ in enumerate(self.dtypes):
            toappend = None if str(typ) in ['float64', 'int64'] else list((self.df[self.dcols[i]]).unique())
            self.unique_vals += [toappend]

    def create_tree(self, temp_df, d = 0):
        if d > self.max_depth:
            node = TreeNode()
            node.data_list = temp_df
            return node
        
        if temp_df.size <= 3:
            node = TreeNode()
            node.data_list = temp_df
            return node
            
        thresh = numpy.var(temp_df['SalePrice'])
        if thresh <= self.min_thresh:
            node = TreeNode()
            node.data_list = temp_df
            return node

        n_t, cond_entropy = len(temp_df), list()

        for i,val in enumerate(self.dcols):
            col_dtype = str(self.dtypes[i])
            numer = set(['float64', 'int64'])
            if col_dtype in numer:
                min_d = temp_df[val].min(axis=None, skipna=None, level=None, numeric_only=None)
                max_d = temp_df[val].max(axis=None, skipna=None, level=None, numeric_only=None)
                step = (max_d - min_d + 1)/self.skip
                classes = []
                for q in range(1,self.skip):
                    classes.append(min_d + q*step)
                classes = [u for u in classes if u <= max_d]
                classes.sort()
                for class_ in classes:
                    mask = temp_df[val] <= class_
                    df_l,df_r = temp_df[mask], temp_df[~mask]

                    n_l, n_r = len(df_l), len(df_r)

                    if n_t == 0:
                        w_l = w_r = 0
                    else:
                        w_l = n_l / n_t
                        w_r = n_r / n_t

                    a_l, a_r = df_l['SalePrice'], df_r['SalePrice']
                    
                    sqrt_var_l,sqrt_var_r = numpy.sqrt(numpy.var(a_l)),numpy.sqrt(numpy.var(a_r))
                    mean_l,mean_r = numpy.mean(a_l),numpy.mean(a_r)
                    s_l,s_r = sqrt_var_l/mean_l,sqrt_var_r/mean_r
                    
                    if numpy.isnan(s_l):
                        s_l = 0
                    if numpy.isnan(s_r):
                        s_r = 0
                    
                    w_var = w_l*s_l+ w_r*s_r
                    cond_entropy += [(w_var, val, class_)]
            
            else:
                classes = self.unique_vals[i]
                for class_ in classes:
                    mask = temp_df[val] == class_
                    df_l, df_r = temp_df[mask], temp_df[~mask]

                    n_l, n_r = len(df_l), len(df_r)

                    if n_t == 0:
                        w_l = w_r = 0
                    else:
                        w_l = n_l / n_t
                        w_r = n_r / n_t

                    a_l, a_r = df_l['SalePrice'], df_r['SalePrice']
                    
                    sqrt_var_l,sqrt_var_r = numpy.sqrt(numpy.var(a_l)),numpy.sqrt(numpy.var(a_r))
                    mean_l,mean_r = numpy.mean(a_l),numpy.mean(a_r)
                    s_l,s_r = sqrt_var_l/mean_l,sqrt_var_r/mean_r
                    
                    if numpy.isnan(s_l):
                        s_l = 0
                    if numpy.isnan(s_r):
                        s_r = 0
                    
                    w_var = w_l*s_l+ w_r*s_r
                    cond_entropy += [(w_var, val, class_)]

        cond_entropy.sort(key=lambda x: x[0])
        splitter = cond_entropy[0]
        
        l_arr,val = [],splitter[2]

        if not str(type(splitter[2])) in ("<class 'str'>"):
            mask = temp_df[splitter[1]] <= splitter[2]
            l_arr.append(self.create_tree(temp_df[mask], d+1))
            l_arr.append(self.create_tree(temp_df[~mask], d+1))
            
        else:
            mask = temp_df[splitter[1]] == splitter[2]
            l_arr.append(self.create_tree(temp_df[mask], d+1))
            l_arr.append(self.create_tree(temp_df[~mask], d+1))

        root = TreeNode()
        root.data_list = root.data_list
        root.attri = splitter[1]
        root.val = val
        root.l_arr = l_arr
        return root

    def train(self, train_filename):
        mae = []
        self.load_data(train_filename)
        self.train_tree(train_filename)
        
    def train_tree(self, train_filename):
        currdata = self.df[:900]
        self.dtree = self.create_tree(currdata)

    def predict(self, TEST_PATH, validate = 0):
        self.test_df = self.df[900:] if validate else pandas.read_csv(TEST_PATH)

        predicted_prices = []

        for i in range(len(self.test_df)):
            curr_node = self.dtree
            
            while curr_node.attri:
                
                if str(type(curr_node.val)) in ("<class 'str'>"):
                    ind = None
                    if self.test_df.iloc[i][curr_node.attri] in [curr_node.val]:
                        if not curr_node.l_arr[0].attri:
                            try:
                                if curr_node.l_arr[0].data_list.empty: ind = 1
                                else: ind = 0
                            except:
                                ind = 0
                        else:
                            ind = 0
                    else:
                        if not curr_node.l_arr[1].attri:
                            try:
                                if curr_node.l_arr[1].data_list.empty: ind = 0
                                else: ind = 1
                            except:
                                ind = 0
                        else:
                            ind = 1
                    curr_node = curr_node.l_arr[ind]

                else:
                    ind = None
                    if not self.test_df.iloc[i][curr_node.attri] > curr_node.val:
                        if not curr_node.l_arr[0].attri:
                            try:
                                if curr_node.l_arr[0].data_list.empty: ind = 1
                                else: ind = 0
                            except:
                                ind = 0
                        else:
                            ind = 0
                    else:
                        if curr_node.l_arr[1].attri:
                            ind = 1
                        else:
                            try:
                                if curr_node.l_arr[1].data_list.empty: ind = 0
                                else: ind = 1
                            except:
                                ind = 1

                    curr_node = curr_node.l_arr[ind]

            sm = numpy.sum(curr_node.data_list['SalePrice'])
            
            predicted_prices += [sm/len(curr_node.data_list['SalePrice'])]
        
        return predicted_prices