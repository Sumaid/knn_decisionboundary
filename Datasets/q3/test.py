from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error,mean_absolute_error

from q3 import DecisionTree as dtree
dtree_regressor = dtree()
dtree_regressor.train('train.csv')
predictions = dtree_regressor.predict('test.csv')
test_labels = list()
with open("test_labels.csv") as f:
  for line in f:
    test_labels.append(float(line.split(',')[1]))
print (mean_absolute_error(test_labels, predictions))

exit(0)

from q1 import KNNClassifier as knc
knn_classifier = knc()
knn_classifier.train('train.csv')
predictions = knn_classifier.predict('test.csv')
test_labels = list()
with open("test_labels.csv") as f:
  for line in f:
    test_labels.append(int(line))
print (accuracy_score(test_labels, predictions))

from q2 import KNNClassifier as knc
knn_classifier = knc()
knn_classifier.train('train.csv')
predictions = knn_classifier.predict('test.csv')
test_labels = list()
with open("test_labels.csv") as f:
  for line in f:
    test_labels.append(line.strip())
print (accuracy_score(test_labels, predictions))

