import numpy as np
import pandas as pd
import heapq as hp
import collections as cl

class KNNClassifier:

    def train(self,filename):
        self.K = 3
        self.dist = lambda x,y : sum(x[i]!=y[i] for i in range(len(x)))
        df = pd.read_csv(filename, header=None)
        self.to_drop = []
        for column in df.columns:
            if df[column].mode()[0] == '?':
                self.to_drop += [column]
                continue
            df[column].replace('?',df[column].mode()[0], inplace=True)
        self.train_df = df.drop(columns=self.to_drop)

    def predict(self,filename):
        predict_df = pd.read_csv(filename, header=None)
        predict_df = predict_df.drop(columns=self.to_drop)
        for column in predict_df.columns:
            predict_df[column].replace('?',predict_df[column].mode()[0], inplace=True)
        predictions = []
        for inp_row in predict_df.itertuples():
            heap = []
            for row in self.train_df.itertuples():
                dis = self.dist(row[2:],inp_row[1:])
                if len(heap) == self.K:
                    hp.heappushpop(heap, (-1*dis, row[1]))
                else:
                    hp.heappush(heap, (-1*dis, row[1]))
            closest_classes = [clos for _,clos in heap]
            predictions += [cl.Counter(closest_classes).most_common(1)[0][0]]
        return predictions