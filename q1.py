from matplotlib import pyplot as plt
plt.plot([2,4,6,8,10,12,14,16,18,20,22],[13,11,8,8,7,5,7,7,14,15,15])
plt.ylabel('Number of online hosts')
plt.xlabel('Time')
plt.show()